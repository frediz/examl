Document: examl
Title: The ExaML v3.0.X Manual
Author: Alexandros Stamatakis
Abstract: <if you have no better clue the short and
 Exascale Maximum Likelihood (ExaML) is a code for phylogenetic inference using
 MPI. This code implements the popular RAxML search algorithm for maximum
 likelihood based inference of phylogenetic trees.
 ExaML is a strapped-down light-weight version of RAxML for phylogenetic
 inference on huge datasets. It can only execute some very basic functions and is
 intended for computer-savvy users that can write little perl-scripts and have
 experience using queue submission scripts for clusters. ExaML only implements
 the CAT and GAMMA models of rate heterogeneity for binary, DNA, and protein
 data.
 ExaML uses a radically new MPI parallelization approach that yields improved
 parallel efficiency, in particular on partitioned multi-gene or whole-genome
 datasets. It also implements a new load balancing algorithm that yields better
 parallel efficiency.
 It is up to 4 times faster than its predecessor RAxML-Light [1] and scales to a
 larger number of processors.
Section: Science/Biology

Format: pdf
Files: /usr/share/doc/examl/ExaML.pdf.gz
